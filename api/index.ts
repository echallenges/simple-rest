import { Server } from '../src/config/server';

const server = new Server('4000');

export default server?.getExpressApp();
