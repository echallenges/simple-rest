import { Nullable } from './../utils';
import { Book } from '../models/Book';
import { BookRepository } from '../models/BookRepository';

export class GetBook {
  private repository: BookRepository;

  constructor({ repository }: { repository: BookRepository }) {
    this.repository = repository;
  }

  async run(bookId: string): Promise<Nullable<Book>> {
    return this.repository.get(bookId);
  }
}
