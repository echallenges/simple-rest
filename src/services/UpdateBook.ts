import { pickBy, identity } from 'lodash';

import { Book } from '../models/Book';
import { BookRepository } from '../models/BookRepository';
import { UpdateBookRequest } from './UpdateBookRequest';

export class UpdateBook {
  private repository: BookRepository;

  constructor({ repository }: { repository: BookRepository }) {
    this.repository = repository;
  }

  async run(bookId: string, request: UpdateBookRequest): Promise<Book> {
    const bookToUpdate = await this.repository.get(bookId);

    if (!bookToUpdate) {
      throw new Error('Book not found');
    }

    const bookProps = {
      ...bookToUpdate,
      ...pickBy(request, identity)
    };

    const book = new Book(bookProps);

    return this.repository.update(book);
  }
}
