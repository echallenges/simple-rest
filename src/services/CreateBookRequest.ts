export interface CreateBookRequest {
  id: string;
  title: string;
  summary: string;
  author: string;
}
