import { Book } from '../models/Book';
import { BookRepository } from '../models/BookRepository';
import { CreateBookRequest } from './CreateBookRequest';

export class CreateBook {
  private repository: BookRepository;

  constructor({ repository }: { repository: BookRepository }) {
    this.repository = repository;
  }

  async run(request: CreateBookRequest): Promise<Book> {
    const book = new Book(request);

    return this.repository.create(book);
  }
}
