export interface UpdateBookRequest {
  title?: string;
  summary?: string;
  author?: string;
}
