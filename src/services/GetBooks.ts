import { Book } from '../models/Book';
import { BookRepository } from '../models/BookRepository';

export class GetBooks {
  private repository: BookRepository;

  constructor({ repository }: { repository: BookRepository }) {
    this.repository = repository;
  }

  async run(): Promise<Book[]> {
    return this.repository.getAll();
  }
}
