export class Book {
  readonly id: string;
  readonly title: string;
  readonly summary: string;
  readonly author: string;

  constructor({ id, title, summary, author }: { id: string; title: string; summary: string; author: string }) {
    this.id = id;
    this.title = title;
    this.summary = summary;
    this.author = author;
  }
}
