import { Nullable } from './../utils';
import { Book } from './Book';

export interface BookRepository {
  create(book: Book): Promise<Book>;
  update(book: Book): Promise<Book>;
  get(bookId: string): Promise<Nullable<Book>>;
  getAll(): Promise<Book[]>;
}
