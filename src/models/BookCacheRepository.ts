import { Nullable } from '../utils';
import { Book } from './Book';

export interface BookCacheRepository {
  set(book: Book): Promise<void>;
  get(bookId: string): Promise<Nullable<Book>>;
  getAll(): Promise<Book[]>;
}
