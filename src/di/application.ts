import { createContainer, asClass, Lifetime } from 'awilix';
import { FileBookRepository } from '../infrastructure/persist/FileBookRepository';
import { CreateBook } from '../services/CreateBook';
import { BookCreateController } from '../controllers/BookCreateController';
import { BookGetController } from '../controllers/BookGetController';
import { GetBook } from '../services/GetBook';
import { InMemoryBookRepository } from '../infrastructure/persist/InMemoryBookRepository';
import { BooksGetController } from '../controllers/BooksGetController';
import { GetBooks } from '../services/GetBooks';
import { SupabaseBookRepository } from '../infrastructure/persist/SupabaseBookRepository';
import { UpdateBook } from '../services/UpdateBook';
import { BookUpdateController } from '../controllers/BookUpdateController';
import { RedisRepository } from '../infrastructure/cache/RedisRepository';
import { MemoryRepository } from '../infrastructure/cache/MemoryRepository';

const env = process.env.NODE_ENV || 'development';

const createAppContainer = () => {
  const container = createContainer();

  container.register({
    cacheRepository:
      env === 'test' ? asClass(MemoryRepository, { lifetime: Lifetime.SINGLETON }) : asClass(RedisRepository),
    repository:
      env === 'production'
        ? asClass(SupabaseBookRepository)
        : env === 'development'
        ? asClass(FileBookRepository)
        : asClass(InMemoryBookRepository, { lifetime: Lifetime.SINGLETON }),
    createBook: asClass(CreateBook),
    updateBook: asClass(UpdateBook),
    getBook: asClass(GetBook),
    getBooks: asClass(GetBooks),
    bookCreateController: asClass(BookCreateController),
    bookUpdateController: asClass(BookUpdateController),
    bookGetController: asClass(BookGetController),
    booksGetController: asClass(BooksGetController)
  });

  return container;
};

export const container = createAppContainer();
