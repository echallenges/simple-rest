import express from 'express';
import errorhandler from 'errorhandler';
import { json, urlencoded } from 'body-parser';
import compress from 'compression';
import * as http from 'http';
import Router from 'express-promise-router';

import { registerRoutes } from '../routes';

export class Server {
  private express: express.Express;
  private port: string;
  private httpServer?: http.Server;

  constructor(port: string) {
    this.express = express();
    this.port = port;

    this.express.use(json());
    this.express.use(urlencoded({ extended: true }));
    this.express.use(compress());

    const env = this.express.get('env') as string;

    const router = Router();
    if (env === 'development') {
      router.use(errorhandler());
    }
    this.express.use(router);

    registerRoutes(router);
  }

  async listen(): Promise<void> {
    return new Promise(resolve => {
      this.httpServer = this.express.listen(this.port, () => {
        console.info(`Listening on port ${this.port}`);
        resolve();
      });
    });
  }

  async stop(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.httpServer?.close(error => {
        if (error) {
          return reject(error);
        }
        return resolve();
      });
    });
  }

  getHttpServer(): Server['httpServer'] {
    return this.httpServer;
  }

  getExpressApp(): Server['express'] {
    return this.express;
  }
}
