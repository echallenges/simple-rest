import { Request, Response } from 'express';

import { Controller } from './Controller';
import { GetBook } from '../services/GetBook';

export class BookGetController implements Controller {
  private getBook: GetBook;

  constructor({ getBook }: { getBook: GetBook }) {
    this.getBook = getBook;
  }

  async run(req: Request, res: Response) {
    try {
      const book = await this.getBook.run(req.params.bookId);

      if (!book) {
        res.status(404).send('Book not found');
        return;
      }

      res.json(book);
    } catch (error) {
      res.status(500).send(error);
    }
  }
}
