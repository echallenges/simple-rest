import { Request, Response } from 'express';

import { Controller } from './Controller';
import { CreateBook } from '../services/CreateBook';

export class BookCreateController implements Controller {
  private createBook: CreateBook;

  constructor({ createBook }: { createBook: CreateBook }) {
    this.createBook = createBook;
  }

  async run(req: Request, res: Response) {
    try {
      const { id, title, summary, author } = req.body;
      const book = await this.createBook.run({ id, title, summary, author });

      res.status(201).json(book);
    } catch (error) {
      res.status(500).send(error);
    }
  }
}
