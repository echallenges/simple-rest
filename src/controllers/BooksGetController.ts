import { Request, Response } from 'express';

import { Controller } from './Controller';
import { GetBooks } from '../services/GetBooks';

export class BooksGetController implements Controller {
  private getBooks: GetBooks;

  constructor({ getBooks }: { getBooks: GetBooks }) {
    this.getBooks = getBooks;
  }

  async run(req: Request, res: Response) {
    try {
      const books = await this.getBooks.run();

      res.json(books);
    } catch (error) {
      res.status(500).send(error);
    }
  }
}
