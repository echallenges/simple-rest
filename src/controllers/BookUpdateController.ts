import { Request, Response } from 'express';

import { Controller } from './Controller';
import { UpdateBook } from '../services/UpdateBook';

export class BookUpdateController implements Controller {
  private updateBook: UpdateBook;

  constructor({ updateBook }: { updateBook: UpdateBook }) {
    this.updateBook = updateBook;
  }

  async run(req: Request, res: Response) {
    try {
      const { title, summary, author } = req.body;
      const id = req.params.bookId as string;

      const book = await this.updateBook.run(id, { title, summary, author });

      res.status(201).json(book);
    } catch (error) {
      res.status(500).send(error);
    }
  }
}
