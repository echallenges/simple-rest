import { Server } from './config/server';

export class Api {
  server?: Server;

  async start(): Promise<void> {
    const port = process.env.PORT ?? '4000';
    this.server = new Server(port);

    return this.server.listen();
  }

  async stop(): Promise<void> {
    return this.server?.stop();
  }

  get httpServer(): Server['httpServer'] | undefined {
    return this.server?.getHttpServer();
  }
}
