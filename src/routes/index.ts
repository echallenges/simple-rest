import { Router, Request, Response, NextFunction } from 'express';
import { body, param, validationResult, ValidationError } from 'express-validator';

import { container } from '../di/application';

export const registerRoutes = (router: Router): void => {
  router.get('/', (req, res) => {
    res.send('Simple API Rest with Node.js, Express, Typescript and Awilix');
  });

  router.get('/books', (req, res) => {
    const booksGetController = container.resolve('booksGetController');
    booksGetController.run(req, res);
  });

  const reqCreateSchema = [
    body('id').exists().isUUID(),
    body('title').exists().withMessage('Should exist').isString().withMessage('Should be a string'),
    body('summary').exists().isString(),
    body('author').exists().isString()
  ];

  router.post('/books', reqCreateSchema, validateReqSchema, (req: Request, res: Response) => {
    const bookCreateController = container.resolve('bookCreateController');
    bookCreateController.run(req, res);
  });

  router.get('/books/:bookId', (req, res) => {
    const bookGetController = container.resolve('bookGetController');
    bookGetController.run(req, res);
  });

  const reqUpdateSchema = [
    param('bookId').exists().isUUID(),
    body('title').optional().isString(),
    body('summary').optional().isString(),
    body('author').optional().isString()
  ];

  router.put('/books/:bookId', reqUpdateSchema, validateReqSchema, (req: Request, res: Response) => {
    const bookUpdateController = container.resolve('bookUpdateController');

    bookUpdateController.run(req, res);
  });

  router.delete('/books/:bookId', (req, res) => {
    const bookGetController = container.resolve('bookGetController');
    bookGetController.run(req, res);
  });
};

function validateReqSchema(req: Request, res: Response, next: NextFunction) {
  const validationErrors = validationResult(req);

  if (validationErrors.isEmpty()) {
    return next();
  }
  const errors = validationErrors.array().map((err: ValidationError) => ({ [err.param]: err.msg }));

  return res.status(422).json({
    errors
  });
}
