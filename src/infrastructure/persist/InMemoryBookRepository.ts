import { BookRepository } from '../../models/BookRepository';
import { Book } from '../../models/Book';
import { Nullable } from '../../utils';

export class InMemoryBookRepository implements BookRepository {
  private books: Book[] = [];

  async create(book: Book): Promise<Book> {
    this.books.push(book);
    return book;
  }

  async update(book: Book): Promise<Book> {
    const bookIndex = this.books.findIndex(book => book.id === book.id);
    this.books.splice(bookIndex, 1, book);
    return book;
  }

  async get(bookId: string): Promise<Nullable<Book>> {
    const book = this.books.find(book => book.id === bookId);
    return book;
  }

  async getAll(): Promise<Book[]> {
    return this.books;
  }
}
