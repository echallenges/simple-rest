import { Nullable } from '../../utils';
import fs from 'fs';
import { serialize, deserialize } from 'bson';

import { BookRepository } from '../../models/BookRepository';
import { Book } from '../../models/Book';
import { BookCacheRepository } from '../../models/BookCacheRepository';

export class FileBookRepository implements BookRepository {
  private FILE_PATH = `${__dirname}/books`;
  private cacheRepository: BookCacheRepository;

  constructor({ cacheRepository }: { cacheRepository: BookCacheRepository }) {
    this.cacheRepository = cacheRepository;
  }

  async create(book: Book): Promise<Book> {
    fs.promises.writeFile(this.filePath(book.id), serialize(book));

    this.cacheRepository.set(book);

    return book;
  }

  async update(book: Book): Promise<Book> {
    fs.promises.writeFile(this.filePath(book.id), serialize(book));

    this.cacheRepository.set(book);

    return book;
  }

  async get(bookId: string): Promise<Nullable<Book>> {
    const cachedBook = await this.cacheRepository.get(bookId);

    if (cachedBook) {
      return cachedBook;
    }

    try {
      const bookData = await fs.promises.readFile(this.filePath(bookId));
      const { id, title, summary, author } = deserialize(bookData);

      return new Book({ id, title, summary, author });
    } catch (error) {
      return null;
    }
  }

  async getAll(): Promise<Book[]> {
    const cachedBooks = await this.cacheRepository.getAll();

    if (cachedBooks.length) {
      return cachedBooks;
    }

    const files = await fs.promises.readdir(this.FILE_PATH);
    const books = await Promise.all(
      files.map(async file => {
        return fs.promises.readFile(`${this.FILE_PATH}/${file}`);
      })
    );

    return books.map(book => {
      const { id, title, summary, author } = deserialize(book);

      return new Book({ id, title, summary, author });
    });
  }

  private filePath(id: string): string {
    return `${this.FILE_PATH}/${id}.repo`;
  }
}
