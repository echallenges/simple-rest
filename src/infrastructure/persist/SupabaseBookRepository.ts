import { createClient } from '@supabase/supabase-js';

import { BookRepository } from '../../models/BookRepository';
import { Book } from '../../models/Book';
import { Nullable } from '../../utils';
import { BookCacheRepository } from '../../models/BookCacheRepository';

export class SupabaseBookRepository implements BookRepository {
  private instance = createClient(process.env['SUPABASE_URL'] as string, process.env['SUPABASE_KEY'] as string);
  private cacheRepository: BookCacheRepository;

  constructor({ cacheRepository }: { cacheRepository: BookCacheRepository }) {
    this.cacheRepository = cacheRepository;
  }

  async create(book: Book): Promise<Book> {
    const { error } = await this.instance.from('books').insert(book);

    if (error) {
      throw error;
    }

    this.cacheRepository.set(book);

    return book;
  }

  async update(book: Book): Promise<Book> {
    const { error } = await this.instance.from('books').update(book).eq('id', book.id);

    if (error) {
      throw error;
    }

    this.cacheRepository.set(book);

    return book;
  }

  async get(bookId: string): Promise<Nullable<Book>> {
    const cachedBook = await this.cacheRepository.get(bookId);

    if (cachedBook) {
      return cachedBook;
    }

    const { data, error } = await this.instance.from('books').select('*').eq('id', bookId).single();

    if (error) {
      throw error;
    }

    return data;
  }

  async getAll(): Promise<Book[]> {
    const cachedBooks = await this.cacheRepository.getAll();

    if (!cachedBooks.length) {
      return cachedBooks;
    }

    const { data, error } = await this.instance.from('books').select('*');

    if (error) {
      throw error;
    }

    return data;
  }
}
