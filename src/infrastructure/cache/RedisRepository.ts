import { Redis } from '@upstash/redis';

import { Book } from '../../models/Book';
import { Nullable } from '../../utils';
import { BookCacheRepository } from '../../models/BookCacheRepository';

const redis = new Redis({
  url: process.env['UPSTASH_REDIS_REST_URL'] as string,
  token: process.env['UPSTASH_REDIS_REST_TOKEN'] as string
});

export class RedisRepository implements BookCacheRepository {
  private instance = redis;

  async set(book: Book): Promise<void> {
    await this.instance.hset('books', {
      [book.id]: JSON.stringify(book)
    });
  }

  async get(bookId: string): Promise<Nullable<Book>> {
    const book = await this.instance.hget('books', bookId);

    if (!book) {
      return null;
    }

    return book as Book;
  }

  async getAll(): Promise<Book[]> {
    const books = await this.instance.hgetall('books');

    return Object.values(books as Record<string, Book>);
  }
}
