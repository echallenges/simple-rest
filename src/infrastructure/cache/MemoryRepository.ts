import { Book } from '../../models/Book';
import { Nullable } from '../../utils';
import { BookCacheRepository } from '../../models/BookCacheRepository';

export class MemoryRepository implements BookCacheRepository {
  private books: Book[] = [];

  async set(book: Book): Promise<void> {
    this.books.push(book);
  }

  async get(bookId: string): Promise<Nullable<Book>> {
    return this.books.find(book => book.id === bookId);
  }

  async getAll(): Promise<Book[]> {
    return this.books;
  }
}
