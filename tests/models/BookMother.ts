import { faker } from '@faker-js/faker';
import { Book } from '../../src/models/Book';

export class BookMother {
  static random(): Book {
    return new Book({
      id: faker.string.uuid(),
      title: faker.lorem.lines(1),
      summary: faker.lorem.paragraph(),
      author: faker.person.fullName()
    });
  }
}
