import { Api } from '../../src/api';

let application: Api;

beforeAll(async () => {
  application = new Api();
  await application.start();
});

afterAll(async () => {
  await application.stop();
});

export { application };
