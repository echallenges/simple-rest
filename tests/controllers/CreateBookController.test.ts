import request from 'supertest';

import { BookMother } from '../models/BookMother';
import { application } from './server';

jest.mock('@upstash/redis');

describe('[POST] /books', () => {
  it('should return 201', async () => {
    const newBookRequest = BookMother.random();
    const response = await request(application.httpServer).post('/books').send(newBookRequest);

    expect(response.status).toBe(201);
  });
});
