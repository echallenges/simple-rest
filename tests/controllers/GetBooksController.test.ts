import request from 'supertest';

import { BookMother } from '../models/BookMother';
import { application } from './server';

jest.mock('@upstash/redis');

describe('[GET] /books', () => {
  it('should return one book', async () => {
    const newBookRequest = BookMother.random();

    await request(application.httpServer).post('/books').send(newBookRequest);

    const responseGet = await request(application.httpServer).get('/books');

    expect(responseGet.body).toEqual([newBookRequest]);
  });
});

export { application };
