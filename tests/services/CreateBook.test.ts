import { CreateBook } from '../../src/services/CreateBook';
import { BookRepositoryMock } from '../__mocks__/BookRepositoryMock';
import { BookMother } from '../models/BookMother';

describe('CreateBook', () => {
  it('should create a book', async () => {
    const repository = new BookRepositoryMock();
    const serviceCreateBook = new CreateBook({ repository });

    const request = BookMother.random();

    await serviceCreateBook.run(request);

    repository.assertCreateHasBeenCalledWith(request);
  });
});
