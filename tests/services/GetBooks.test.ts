import { GetBooks } from '../../src/services/GetBooks';
import { BookRepositoryMock } from '../__mocks__/BookRepositoryMock';
import { BookMother } from '../models/BookMother';

describe('GetBooks', () => {
  it('should return books', async () => {
    const repository = new BookRepositoryMock();
    const getBooks = new GetBooks({ repository });

    const newBooks = [BookMother.random(), BookMother.random()];

    repository.returnOnSearchAll(newBooks);

    const books = await getBooks.run();

    repository.assertSearchAllHaveBeenCalled();

    expect(books).toEqual(newBooks);
  });
});
