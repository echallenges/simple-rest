import { UpdateBook } from '../../src/services/UpdateBook';
import { BookRepositoryMock } from '../__mocks__/BookRepositoryMock';

describe('UpdateBook', () => {
  it('should update a book', async () => {
    const repository = new BookRepositoryMock();
    const serviceUpdateBook = new UpdateBook({ repository });

    const request = {
      id: '1',
      title: 'title',
      summary: 'summary',
      author: 'author'
    };

    repository.returnOnSearchAll([request]);

    await serviceUpdateBook.run(request.id, { title: 'new title' });

    repository.assertUpdateHasBeenCalledWith({
      ...request,
      title: 'new title'
    });
  });
});
