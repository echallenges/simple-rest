import { Nullable } from './../../src/utils';
import { Book } from '../../src/models/Book';
import { BookRepository } from '../../src/models/BookRepository';

export class BookRepositoryMock implements BookRepository {
  private mockCreate: jest.Mock;
  private mockUpdate: jest.Mock;
  private mockSearchAll: jest.Mock;
  private books: Array<Book> = [];

  constructor() {
    this.mockCreate = jest.fn();
    this.mockUpdate = jest.fn();
    this.mockSearchAll = jest.fn();
  }

  async create(book: Book): Promise<Book> {
    this.books.push(book);
    return this.mockCreate(book);
  }

  async update(book: Book): Promise<Book> {
    return this.mockUpdate(book);
  }

  async get(bookId: string): Promise<Nullable<Book>> {
    return this.books.find(book => book.id === bookId);
  }

  async getAll(): Promise<Book[]> {
    this.mockSearchAll();
    return this.books;
  }

  returnOnSearchAll(books: Array<Book>) {
    this.books = books;
  }

  assertSearchAllHaveBeenCalled() {
    expect(this.mockSearchAll).toHaveBeenCalled();
  }

  assertCreateHasBeenCalledWith(expected: Book) {
    expect(this.mockCreate).toHaveBeenCalledWith(expected);
  }

  assertUpdateHasBeenCalledWith(expected: Book) {
    expect(this.mockUpdate).toHaveBeenCalledWith(expected);
  }
}
