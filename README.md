# Simple Rest

This code was developed as part of a job application challenge. It is basically a rest api to allow get and add books.

## Technologies and libraries

- Nodejs
- Typescript
- Redis
- Supabase
- Expressjs
- Vercel
- Upstash
- Awilix
- Jest
- Prettier
- Eslint

## APIs

This challenge has four apis:

- `[GET] /books`: get list of books
- `[GET] /books/:bookId`: get book detail
- `[POST] /books`: add new book
- `[PUT] /books/:bookId`: update book

### Example of Book

```
{
    "id": "550e8400-e29b-41d4-a716-446655440001",
    "title": "Title New 2",
    "summary": "Lorem ipsum",
    "author": "Marcus"
}
```
